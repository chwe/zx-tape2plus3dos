# Copyright 2024 TIsland Crew
# SPDX-License-Identifier: Apache-2.0

SRCS = t2p3.asm plus3.asm src/fname.asm src/s-cmp.asm

t2p3.tap: $(SRCS)
	sjasmplus --nologo --msg=war $(OPTS) t2p3.asm

# EOF vim: et:ai:ts=4:sw=4:

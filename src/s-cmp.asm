; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

SCMP:	; HL, - tmpl ; DE - string
        ld bc, 10
.scan:  ld a, (de)
        cpi
        ret nz  ; ZF=1, NO match
        inc de
        ld a, b
        or c
        jr nz, .scan
        ret     ; ZF=0, equals

; EOF vim: et:ai:ts=4:sw=4:

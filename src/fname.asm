; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

name:
        ld hl, TAP_NAME
        ld de, fname
        ld b, 8
.pname:
        ld a, (hl)
        cp ' '
        jr z, .done
        cp '.'
        jr z, .delim
        ld (de), a
        inc hl
        inc de
        djnz .pname
.suffix:
        ld a, '.'
        ld (de), a
        inc de
        ld a, 2     ; TAP name is 10 chars long
        add a, b
        ld b, a
        ld c, 4     ; +3 TYPE is 3 chars long, we use SHIFT RIGHT to track
.psfx:
        ld a, (hl)
        cp ' '
        jr z, .done
        ld (de), a
        inc hl
        inc de
        rrc c
        jr c, .done
        djnz .psfx
.done:
        ld a, 0
        ld (de), a
        ret
.delim:
        inc hl
        dec b
        jr .suffix

; EOF vim: et:ai:ts=4:sw=4:

; Copyright 2025 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

; A simple way to wrap a file in a TAP container
; this is a very limited approach, file size must be < 32769 bytes
; Obviously, requires sjasmpls :)
; call as: sjasmplus -DFILE=file-to-wrap file2tap.asm
; produces file-to-wrap.tap

    DEVICE ZXSPECTRUM128

    ; arbitrary address
    DEFINE ADDRESS 0x6000

    LUA
        sj.insert_define("TAPFILE", '"'..sj.get_define("FILE")..".tap\"")
        sj.insert_define("TAPNAME", '"'..string.sub(sj.get_define("FILE"),1,10)..'"')
    ENDLUA

    ORG ADDRESS

start:
    INCBIN FILE

    EMPTYTAP TAPFILE
    SAVETAP  TAPFILE,CODE,TAPNAME,ADDRESS,$-start

; EOF vim: et:ai:ts=4:sw=4:

; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

    DEVICE ZXSPECTRUM128

    IFDEF BUILD_TYPE_REGULAR    ; traditional BASIC loader + CODE block

    MODULE BASIC
        ORG 23755
        INCLUDE "BasicLib.asm"  ; examples/BasicLib/BasicLib.asm
_START:
        LINE : db cls,':',rand,usr : NUM 24576 : defb ':',goto : NUM 9999 : LEND
        LINE : db clear : NUM 24575 : LEND
        LINE : db load,'"t2p3.bin"',code : LEND
        LINE : db goto : NUM 10 : LEND
_END    equ $

    ENDMODULE ; BASIC

        org 0x6000  ; FIXME: can be higher since we support proper chunks

    ELSE ;!BUILD_TYPE_REGULAR - single BASIC file with CODE in REM

    MACRO NUM label, value
        db label
        db 0x0E, 0x00, 0x00 : dw value : db 0x00
    ENDM ;NUM

    MACRO PRINT0 msg
        ld hl, msg
        call printhlz
    ENDM ;PRINT0

RAMTOP  equ 0x63ff
ADDRESS equ RAMTOP+1

        ORG 23755

_begin:
    dw 0x0100, _end-$-4 ; BASIC line 1 + length
    db 0xFD, '0'    ; CLEAR RAMTOP
        db 0x0E, 0x00, 0x00: dw RAMTOP : db 0x00
    db ':', 0xF9, 0xC0, '('  ; : RANDOMIZE USR (
    db '0'  ; .loader offset from BASIC start
        db 0x0E, 0x00, 0x00 : dw .loader-_begin : db 0x00
    db '+', 0xBE, '0'   ; + PEEK 23635
        db 0x0E, 0x00, 0x00, 0x53, 0x5C, 0x00
    db '+', '0'         ; + 256
        db 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00
    db '*', 0xBE, '0'   ; * PEEK 23636
        db 0x0E, 0x00, 0x00, 0x54, 0x5C, 0x00
    db ')', ':', 0xEA   ; ) : REM

.loader:
        ; BC - .loader per BASIC USR contract
        ld hl, .code-.loader
        add hl, bc
        ld de, ADDRESS
        ld bc, _end-.code
        push de ; save launch address on stack
        ldir
        ret     ; return to launch address (was in DE)
.code:
    DISPLAY "Bootsrap size:",/A,.code-_begin

    DISP ADDRESS  ; FIXME: can be higher since we support proper chunks

    ENDIF;BUILD_TYPE_REGULAR

outputBuffer    equ 0x7000  ; 36864 (36K)
    assert outputBuffer > RAMTOP

    ; data preparation (load address 0 is the LAST CHUNK marker):
    ; - single chunk:
    ;   tapcat -K tx.tap -m0 SOURCE
    ; - multiple chunks:
    ;   tapcat -K tx.tap -m2 CHUNK1 -m1 CHUNK2 -m0 CHUNK3
    ; - meta data block with file name:
    ;   echo "filename.typ" > _P3FN-TAG_
    ;   tapcat -K tx.tap -m0 _P3FN-TAG_ -m0 SOURCE
    ; - multiple blocks with "pause"/"delay"
    ;   echo "delay" > _delay
    ;   tapcat -K tx.tap -m1 CHUNK2 -d _delay -m0 CHUNK1
    ; NOTE: "vanilla" tapcat REMOVES PLUS3DOS headers!!! Use bin2tap
    ; since you need to preserve those (or my own tapcat which understands -K).
    ;
    ; of course, you can combine multiple files/chunks into a single TAP
    ; but since disk operations aren't exactly fast on +3, we may want to
    ; add "pauses" (BASIC or an array dummy files) between actual data fragments
    ; Hint: you can use https://worldofspectrum.org/zxplus3e/software.html#taprom
    ; as poor man's acrhive container to transfer multiple files or 
    ; use proper PMARC/PMEXT under CP/M

_start:
        ld a, 2     ; upper/main screen channel
        call 0x1601 ; CHAN-OPEN - https://skoolkid.github.io/rom/asm/1601.html

main:
        PRINT0 s_waiting

hdr:
        call 0x1f54 ; BREAK-KEY https://skoolkid.github.io/rom/asm/1F54.html
        ; CF=0 - BREAK pressed (A NOT preserved)
        ret nc
        ld a, 0x00  ; header block
        ld de, 17   ; block length
        ld ix, header
        scf         ; CF=1 - load
        call 0x0556 ; LD-BYTES https://skoolkid.github.io/rom/asm/0556.html
        jr nc, hdr   ; CF=0, error

        ld a, (TAP_TYPE)
        cp 3        ; consider only CODE blocks
        jr nz, hdr

        ld a, (META)    ; "METADATA AVAILABLE" flag
        or a
        jr nz, .metaok  ; do we have meta data preloaded?
        call meta_data_blk  ; check for _P3FN-TAG_
        jr c, hdr   ; loaded meta data block, load the first data block
        call name   ; no meta, convert header.name -> fname

.metaok:
        ld hl, s_block
        call report

        ; check block size
        ld de, (TAP_LENGTH)
        ld hl, 0xffff-outputBuffer+1
        or a    ; ensure CF=0
        sbc hl, de
        jr nc, .data
        PRINT0 s_too_big
        jr hdr

.data:
        ld a, 0xff  ; data block
        ld de, (TAP_LENGTH)    ; block length
        ld ix, outputBuffer
        scf         ; CF=1 - load
        call 0x0556 ; LD-BYTES https://skoolkid.github.io/rom/asm/0556.html
        jr nc, hdr  ; CF=0, error

        ld hl, s_writing
        call report

        ld a, (FILE)        ; "OPEN FD"
        or a
        jr nz, .writeblock  ; if we have file opened, append data

        ld b, Dos.FMODE_CREATE
        ld hl, fname
        call Dos.fopen
        ld a, Dos.FILENO
        ld (FILE), a

.writeblock:
        ld bc, (TAP_LENGTH)  ; length
        ld hl, outputBuffer
        call Dos.fwrite
        ; FIXME: error check?

        PRINT0 s_clear

        ld hl, s_report
        call report

        ; if load address < 0x0100 wait until it's 0, appending all chunks to a single file
        ld hl, (TAP_PARAM1)
        ld a, h
        or a
        jr nz, .close:
        or l
        jp nz, hdr

.close:
        call Dos.fclose
        xor a
        ld (FILE), a    ; clear "OPEN FD"
        ld (META), a    ; clear "METADATA AVAILABLE"

        jp main

meta_data_blk:  ; on exit: CF=1 - meta block detected, CF=0 - regular block
        ld hl, p3_file_tag
        ld de, TAP_NAME
        call SCMP
        ret nz          ; ZF=0 no match thus CF=0 by definition after OR
        ; load meta block
        PRINT0 s_waiting
        PRINT0 s_metadata
        ld a, 0xff      ; data block
        ld de, (TAP_LENGTH)    ; block length
        ld ix, outputBuffer
        scf             ; CF=1 - load
        call 0x0556     ; LD-BYTES https://skoolkid.github.io/rom/asm/0556.html
        ret nc          ; CF=0, error
        ; copy file name
        ld hl, outputBuffer
        ld de, fname
        ld b, 8+1+3 ; max fname length
.scan:  ld a, (hl)
        cp 32   ; shall it be 33?
        jr c, .exit
        ld (de), a
        inc hl
        inc de
        djnz .scan
.exit:  xor a
        ld (de), a  ; NULL-terminate file name
        ld a, 0xff
        ld (META), a; set "METADATA AVAILABLE" flag
        PRINT0 s_waiting
        ld hl, fname
        call printhlz
        scf
        ret

report:
        call printhlz
        PRINT0 fname
        ld a, ' '
        rst 0x10
        ld bc, (TAP_LENGTH)
        call printnum
        ; TODO: print chunk #
        ld a, 6
        rst 0x10
        ret

printhlz:
        ld a, (hl)
        or a
        ret z
        rst 0x10
        inc hl
        jr printhlz

printnum:   ; BC - number
        call 0x2d2b ; STACK-BC https://skoolkid.github.io/rom/asm/2D2B.html
        jp   0x2de3 ; PRINT A FNUM https://skoolkid.github.io/rom/asm/2DE3.html
        ;ret

    include "src/fname.asm"
    include "src/s-cmp.asm"
    include "plus3.asm"

s_waiting:  defb 22, 2, 1, 'Loading...', 0
s_block:    defb 22, 2, 1, 'File ', 0
s_writing:  defb 22, 2, 1, 'Writing ', 0
s_too_big:  defb 22, 3, 1, 'TOO BIG!', 0
s_report:   defb 22, 1, 1, 0
s_clear:    defb 22, 2, 1, 6, 6, 6, 6, 0
s_metadata: defb ' meta data', 0
p3_file_tag:defb '_P3FN-TAG_'
FILE:   defb 0  ; this one must be initialised with 0
META:   defb 0  ; this one must be initialised with 0
    IFNDEF BUILD_TYPE_REGULAR
        db 0x0d ; BASIC line end
__BUILD_TYPE_BUNDLE_END equ $   ; keep track of the runtime address
        ENT ; DISP ADDRESS
    ENDIF ;BUILD_TYPE_REGULAR
_end    equ $   ; no need to pre-allocate memory for empty buffers
    IFNDEF BUILD_TYPE_REGULAR
        ; but we need to record the correct addresses...
        DISP __BUILD_TYPE_BUNDLE_END
    ENDIF ;BUILD_TYPE_REGULAR
header  equ TAP_TYPE
TAP_TYPE:   defb 0  ; 00 1 - type (0 - BAS, 1 - NumArr, 2 - CharAr, 3 - CODE)
TAP_NAME:   defs 10 ; 01 10 - name (space padded)
TAP_LENGTH: defw 0  ; 11 2 - length
TAP_PARAM1: defw 0  ; 13 2 - parameter 1 (bas: autostart line | code: load address)
TAP_PARAM2: defw 0  ; 15 2 - parameter 2 (bas: vars | code: 32768)
        
fname:  defs 8+1+3+1    ; 8.3[0xff]
    IFNDEF BUILD_TYPE_REGULAR
        ; making sure all the variables above have correct addresses
        ; and now swiching back to the compile-time addresses...
        ENT ; __BUILD_TYPE_BUNDLE_END
    ENDIF ;BUILD_TYPE_REGULAR

    ASSERT $ < outputBuffer ; FIXME: wrong for undefined BUILD_TYPE_REGULAR

    DEFINE TAPE "t2p3.tap"
    EMPTYTAP TAPE
    IFDEF BUILD_TYPE_REGULAR
        SAVETAP  TAPE,BASIC,"T2P3",BASIC._START,BASIC._END-BASIC._START,20
        SAVETAP  TAPE,CODE,"t2p3.bin",_start,_end-_start
    ELSE ;BUILD_TYPE_REGULAR
        SAVETAP  TAPE,BASIC,"t2+3",_begin,_end-_begin,0
    ENDIF;BUILD_TYPE_REGULAR

; EOF vim: et:ai:ts=4:sw=4:

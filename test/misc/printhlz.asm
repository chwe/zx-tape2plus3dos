; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

printhlz:
        push hl
.print: ld a, (hl)
        or a
        jr z, .exit
        rst 0x10
        inc hl
        jr .print
.exit:  pop hl
        ret

; EOF vim: et:ai:ts=4:sw=4:

; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

print10:
        push hl
        ld b, 10
.print: ld a, (hl)
        rst 0x10
        inc hl
        djnz .print
        ld a, ':'
        rst 0x10
        pop hl
        ret

; EOF vim: et:ai:ts=4:sw=4:

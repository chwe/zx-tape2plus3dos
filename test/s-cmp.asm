; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

; sjasmplus -DUTST_ASM_SRC=test/s-cmp.asm -DUTST_OUT=\"test.tap\" test/_.asm
; fuse-emulator -m plus3 test.tap --plus3disk plus3.dsk

        ld hl, TAG
        ld de, L1
        call CMPTEST

        ld hl, TAG
        ld de, L2
        call CMPTEST

        ld hl, TAG
        ld de, L3
        call CMPTEST

        ld hl, TAG
        ld de, L4
        call CMPTEST
        ret

CMPTEST:
        call print10
        ex de, hl
        call print10
        ex de, hl
        call SCMP
        ld a, 'M'
        jr z, .m1
        ld a, '-'
.m1:    rst 0x10
        ld a, 13
        rst 0x10
        ret

    INCLUDE "test/misc/print10.asm"

TAG:    defb '_P3FN-TAG_'
L1:     defb '_P3FN-TAG_'
L2:     defb 'some.file '
L3:     defb '_P3nowhere'
L4:     defb ' s o m e j'

    INCLUDE "src/s-cmp.asm"

; EOF vim: et:ai:ts=4:sw=4:

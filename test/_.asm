; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

;
    ; sjasmplus -DUTST_ASM_SRC="\"whatever.asm\""
    IFNDEF UTST_ASM_SRC
        LUA
            sj.error("UTST_ASM_SRC is not defined! Nothing to test!")
        ENDLUA
    ENDIF ;UTST_ASM_SRC
    IFNDEF UTST_OUT
        ;DISPLAY "ERROR: UTST_OUT is not defined!!!"
        LUA
            sj.error("UTST_OUT is not defined!!!")
        ENDLUA
    ENDIF ;UTST_OUT

    DEVICE ZXSPECTRUM48
    ORG 0x8000
    DEFINE UNIT_TEST

_utest:
        ld a, 2     ; upper/main screen channel
        call 0x1601 ; CHAN-OPEN - https://skoolkid.github.io/rom/asm/1601.html
        ; unit test code here
_utest_impl_start equ $

    IFDEF UTST_ASM_SRC
        INCLUDE UTST_ASM_SRC
    ENDIF;UTST_ASM_SRC

;
    IF 0 == ($-_utest_impl_start)
        LUA
            sj.error("ERROR: no unit test code?")
        ENDLUA
    ENDIF

PUTS:   ; print 0-terminated string (HL)
        push hl
.loop:
        ld a, (hl)
        or a
        jr z, .exit
        rst 0x10
        inc hl
        jr .loop
.exit:
        pop hl
        ret

    IFDEF UTST_OUT
        EMPTYTAP UTST_OUT
        SAVETAP  UTST_OUT,CODE,"ut-code",_utest,$-_utest
        DISPLAY  UTST_OUT
    ENDIF;UTST_OUT

; EOF vim: et:ai:ts=4:sw=4:

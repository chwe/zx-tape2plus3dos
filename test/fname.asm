; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

; sjasmplus -DUTST_ASM_SRC=test/fname.asm -DUTST_OUT=\"test.tap\" test/_.asm
; fuse-emulator -m plus3 test.tap --plus3disk plus3.dsk

        ld hl, S1
        call FNAME_TEST

        ld hl, S2
        call FNAME_TEST

        ret

FNAME_TEST: ; HL - TAP name
        call LDTAP_NAME

        call name

        ld hl, fname
        call printhlz
        ld a, 13 : rst 0x10

        ret

LDTAP_NAME:
        call print10
        ld de, TAP_NAME
        ld bc, 10
        ldir
        ret

    INCLUDE "src/fname.asm"
    INCLUDE "test/misc/print10.asm"
    INCLUDE "test/misc/printhlz.asm"

S1:         defb 'examplept3'
S2:         defb 'AY@576.P3 '
TAP_NAME:   defb '1234567890'
fname:      defs 8+1+3+1
_:          defb 0

; EOF vim: et:ai:ts=4:sw=4:

; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

    IFNDEF __MODULE_DOS_PLUS3_ASM__
    DEFINE __MODULE_DOS_PLUS3_ASM__

__MODULE_DOS_PLUS3_ASM_START equ $

    MODULE Dos

    ; naming is a bit weird, but copied directly from
    ; The Sinclair ZX Spectrum +3 manual, copyright Amstrad Plc.
bankm        equ 5B5Ch  ;system variable that holds the last value output to 7FFDh
port1        equ 7FFDh  ;address of ROM/RAM switching port in I/O map

; https://worldofspectrum.org/ZXSpectrum128+3Manual/chapter8pt27.html

DOS_OPEN    equ 0x0106  ; Open file
DOS_CLOSE   equ 0x0109  ; Close file
DOS_ABANDON equ 0x010C  ; Abandon file (force it closed)
DOS_READ    equ 0x0112  ; Read data
DOS_WRITE   equ 0x0115  ; Write data
DOS_GET_POSITION    equ 0x133   ; Get the file pointer (000000h...FFFFFFh)
DD_L_OFF_MOTOR  equ 0x019C  ; Turn off motor

FILENO  equ 0x07    ; arbitrary number used as file descriptor

; "Open Action"
OA_HDR_USE  equ 1   ; Open the file, read the header (if any). Position file
                    ; pointer after header.
OA_HDR_IGN  equ 2   ; Open the file, ignore any header. Position file pointer
                    ; at 000000h (0).
OA_BCKUP    equ 3   ; Assume given filename is 'filename.type'. Erase
                    ; 'filename.BAK' (if it exists). Rename 'filename.type' to
                    ; 'filename.BAK'. Follow create action.
OA_ERASE    equ 4   ; Erase existing version. Follow create action.

; "Create Action"
CA_USE_HDR  equ 1   ; Create and open new file with a header. Position file
                    ; pointer after header.
CA_NO_HDR   equ 2   ; Create and open new file without a header. Position file
                    ; pointer at 000000h (0).

; Access Mode
_MODE_RD    equ 1
_MODE_WR    equ 2
_MODE_RDRW  equ 3

; Puiblic API
FMODE_READ      equ 1
FMODE_WRITE     equ 2
FMODE_CREATE    equ 4

; self-modified code marker, these values always overwritten at runtime
_SMC_W_   equ 0xdead    ; FIXME: use DEFINE and UNDEFINE at the end of the module?
_SMC_B_   equ 0x5A

    MACRO PLUS3CALL api
        call plus3api
        dw api
    ENDM

; NOTE: '-' is NOT a valid character for +3 file name! " and ' ARE...
; Allowed characters are
;   [A-Z][a-z]
;   [0-9]
;   "#$'@^_{}~£

loadBuffer:     ; HL - filename in ASCIIZ
    ; copied verbatim from dos/esxdos.asm
        ld b, FMODE_READ
        call fopen
        ret nc
        ld hl, outputBuffer
        ld bc, 0xffff-outputBuffer
        call fread
        ; FIXME: check CF
        ld hl, outputBuffer : add hl, bc : xor a : ld (hl), a : inc hl : ld (hl), a
        call fclose
        ret

; for 3DOS operations code selects FD it wants to use (0..15)
; hence, we ignore FD argument and use hardcoded FILENO

        ; Open file using default drive
fopen:  ; B - file mode, HL - file name ; Return: A - fd
        ;call P3_EOFN    ; replace terminating \0 with 0xff
        call P3_FN_CNV
        ld a, FMODE_CREATE
        cp b
        jr z, .create
        ld a, FMODE_WRITE
        cp b
        jr z, .write
        ld c, _MODE_RD
        ld de, 0x0002   ; open if exists
        jr .open
.write:
        ld c, _MODE_WR
        ld de, 0x0204   ; create new, no headers
        jr .open
.create:
        ld c, _MODE_WR
        ld de, 0x0204   ; create new, no headers
.open:
        ld b, FILENO
        ; 0106  DOS_OPEN
        ; B - file number (0..15)
        ; C - Access mode: bits 1-rd,2-wr,3-rdwr,5-shared_rdwd
        ; D - Create action
        ; E - Open action
        ; HL - 0xFF terminatd file name
        ; A, BC DE HL IX corrupt, all other registers preserved.
        PLUS3CALL DOS_OPEN  ; BC DE HL IX corrupt
        ; CF=1 -- success, A corrupt; CF=0 -- error, A - error code
        ; new file created:     CF=1 ZF=1
        ; existing file opened: CF=1 ZF=0
        ret c
        push af
        ld b, FILENO
        PLUS3CALL DOS_ABANDON       ; BC DE HL IX corrupt, all other registers preserved.
        ; explicitly turn off motor, MRF disables interrupts?
        PLUS3CALL DD_L_OFF_MOTOR    ; AF BC DE HL IX corrupt, all other registers preserved.
        pop af  ; CF=0 A- error code
        ret

fclose: ; A - fd
        ld b, FILENO
        ; 0109  DOS_CLOSE
        ; B - File number (0..15)
        PLUS3CALL DOS_CLOSE ; BC DE HL IX corrupt
        ; CF=1 - success, A corrupt; CF=0 - error, A - error code
        push af
        jr c, .exit
        ld b, FILENO
        PLUS3CALL DOS_ABANDON       ; BC DE HL IX corrupt, all other registers preserved.
        ; explicitly turn off motor, MRF disables interrupts?
.exit:  PLUS3CALL DD_L_OFF_MOTOR    ; AF BC DE HL IX corrupt, all other registers preserved.
        pop af  ; CF=0 A - error code
        ret

fread:  ; A - fd, BC - len, HL - ptr ; Return: BC - nread
        push bc
        push hl
        ; get current position, to calculate the number of read bytes later
        ld b, FILENO
        ; 0133 DOS_GET_POSITION
        ; B - File number (0..15)
        PLUS3CALL DOS_GET_POSITION  ; BC D IX corrupt
        ; CF=1 - succes, A NOT preserved, E HL - File pointer
        ; CF=0 - error, A - error code, E HL corrupt
        ld (.save_fpos), hl ; we only use lower 16 bits of 24 bits FPOS
        pop hl  ; ptr
        pop de  ; number of bytes to read
        ret nc  ; cannot get position -- error. AFTER restoring stack!
        ld b, FILENO    ; file number/fd
        ld a, (bankm)
        and 0x07
        ld c, a         ; keep already selected page
        ; HL is the buffer pointer already
        ; 0112  DOS_READ
        ; B - File number (0..15)
        ; C - Page number for 0xC000..0xFFFF
        ;       only used when HL is within the upper 16K of memory (0xC000…0xFFFF). If HL is in this range then C is the RAM bank to use. For most purposes C should be set to 0 which is the default RAM bank mapped to that region. 
        ; DE - Number of bytes to read, 0 means 64K
        ; HL - Address where to write the bytes to
        ; BC HL IX corrupt, all other registers preserved.
        PLUS3CALL DOS_READ  ; BC HL IX corrupt
        ; CF=1 - success; CF=0 - error, A - error, DE - unread/remaining bytes
        ; at exit BC is expected to have the number of bytes actually read
        ; so we query the current file pointer and return its value
        ld b, FILENO
        ; 0133 DOS_GET_POSITION
        ; B - File number (0..15)
        PLUS3CALL DOS_GET_POSITION  ; BC D IX corrupt
        ; CF=1 - succes, A NOT preserved, E HL - File pointer
        ; CF=0 - error, A - error code, E HL corrupt
        ret nc
.save_fpos  equ $ + 1
        ld bc, _SMC_W_
        or a    ; CF=0
        sbc hl, bc
        push hl
        pop bc
        scf ; CF=1 - success
        ret

fwrite: ; A - fd, BC - len, HL - ptr ; Return: BC - nwritten
        push bc
        pop de  ; number of bytes to write
        ld b, FILENO    ; file number/fd
        ld a, (bankm)
        and 0x07
        ld c, a         ; keep already selected page
        ; hl already points to the buffer
        ; 0115  DOS_WRITE
        ; B - File number (0..15)
        ; C - Page number for address 0xC000-0xFFFF
        ;       only used when HL is within the upper 16K of memory (0xC000…0xFFFF). If HL is in this range then C is the RAM bank to use. For most purposes C should be set to 0 which is the default RAM bank mapped to that region. 
        ; DE - Number of bytes to write, 0 means 64K
        ; HL - Address where to read the bytes from
        ; BC HL IX corrupt, all other registers preserved.
        PLUS3CALL DOS_WRITE ; BC HL IX corrupt
        ; CF=1 - success (A, DE corrupted)
        ; CF=0 - error, A - error, DE - Number of bytes remaining unwritten
        ret

plus3api:   ; +3DOS Trampoline, usage: call plus3api : dw DOS_OPEN
        ld (.save_de), de
        ex (sp), hl
        ld e, (hl)
        inc hl
        ld d, (hl)
        inc hl
        ex (sp),hl
        ld (.call), de
.save_de    equ $ + 1
        ld de, _SMC_W_
        call PG_IN_DOS
.call       equ $ + 1
        call _SMC_W_
        ;jr PG_IN_USR0

; https://worldofspectrum.org/ZXSpectrum128+3Manual/chapter8pt26.html
PG_IN_USR0: ; page in 48k ROM
        push af
        push bc
        ; we restore previously selected page, PG_IN_DOS stores bankm here
save_bankm equ $ + 1
        ld a, _SMC_B_
PG_SWITCH:
        ld bc, port1
        di
        ld (bankm), a
        out (c), a
        ei
        pop bc
        pop af
        ret
; https://worldofspectrum.org/ZXSpectrum128+3Manual/chapter8pt26.html
;   Calling +3DOS from BASIC
; "[...] DOS can only be called with RAM page 7 switched in at
; the top of memory, the stack held somewhere in that range
; 4000h...BFE0h, and ROM 2 (the DOS ROM) switched in at the bottom of
; memory (000h...3FFFh)."
PG_IN_DOS:  ; TODO: combine, prologue is the same, only set/res and/or is diff
        push af
        push bc
        ld a, (bankm)
        ld (save_bankm), a  ; preserve to restore later
        ; we keep bit 3, normal(0)/shadow(1) screen
        res 4, a    ; Bit 4 is the low bit of the ROM selection.
        or 0x07     ; 0000 0111
        jr PG_SWITCH

    ; compat shim only, if we target +3 nothing prevents us from using 0xFF as terminator :)
; P3_EOFN:    ; HL - zstring; replace \0 with 0xFF terminator; A - NOT preserved
;         push hl
;         push bc
;         xor a
;         ld b, a
;         ld c, a
;         cpir    ; HL now points to the past the NULL terminator
;         dec hl
;         ld (hl), 0xff
;         pop bc
;         pop hl
;         ret

P3_FN_CNV:  ; HL -zstring; convert file path to +3 name (8+3); return - HL converted +3 name
    ; A, HL - NOT preserved; BC, DE - preserved
        push bc, de
.sname: ld de, .P3_FNAME    ; dst buffer
        ld b, 8         ; max 8 characters in the file name
.name:  ld a, (hl)
        inc hl
        or a            ; end of source name?
        jr z, .done
        cp '.'          ; end of name, switching to type?
        jr z, .type
        cp '/'          ; path sep, start over, that was long dir name
        jr z, .sname
        ld (de), a
        inc de
        djnz .name
.skipn: ld a, (hl)      ; name is longer than 8 chars, skip the rest
        inc hl
        or a
        jr z, .done
        cp '.'          ; end of name, switching to type?
        jr z, .type
        cp '/'          ; path sep, start over, that was long dir name
        jr z, .sname
        jr .skipn
.type:  ld (de), a      ; store name-type separator '.'
        inc de
        ld b, 3         ; max type length 3 chars
.tprep: ld a, (hl)
        inc hl
        or a            ; end of source path?
        jr z, .done
        ld (de), a
        inc de
        djnz .tprep
.done:  ld a, 0xff
        ld (de), a
        pop de, bc
        ld hl, .P3_FNAME
        ret

.P3_FNAME:  defs 8+1+3+1    ; 8'.'3[0xFF]

    ENDMODULE;Dos

    IFDEF _MODULE_MMAP_TRACE
    DISPLAY "DOS:PLUS3@", __MODULE_DOS_PLUS3_ASM_START, " size:", /A, $-__MODULE_DOS_PLUS3_ASM_START
    ENDIF;_MODULE_MMAP_TRACE


    ENDIF ;__MODULE_DOS_PLUS3_ASM__

; EOF vim: et:ai:ts=4:sw=4:

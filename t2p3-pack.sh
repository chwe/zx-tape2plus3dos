#!/bin/bash

# This needs MODIFIED tapcat, the one that understands -K switch

set -e

if [[ -w /dev/shm ]]; then
    export TMPDIR=/dev/shm
fi

cleanup() {
    (
        cd "$WDIR"
        find . -type f -exec rm \{\} \;
    )
    rmdir "$WDIR"
}

tapedelay() { # SIZE
    local DELAY="$WDIR/_delay"
    local count=$(( $1 * 128 / 2048 )) # 14MHz 4096, 7Mhz 2048, 3.5 1024
    if [[ $count -gt 0 ]]; then
        yes | head -$count > "$DELAY"
        tapcat "$TAP" -d "$DELAY" 2>&1 >/dev/null
    fi
}

nametag() {
    local TAG="$WDIR/_P3FN-TAG_"
    echo "$1" > "$TAG"
    tapcat -K "$TAP" -m0 "$TAG"
    test -f "$TAG" && rm "$TAG"
}

tapefy() { # file
    local f=$(basename "$1")
    if [[ ${#f} -gt 10 ]]; then
        nametag "$f"
    fi
    tapcat -K "$TAP" -m0 "$1"
}

multitape() { # file
    split --bytes=32768 "$1" "$WDIR/chnk."
    nametag "$1"
    local count=$(find "$WDIR" -type f -name 'chnk.*' | wc -l)
    count=$(( $count - 1 )) # count down to 0
    while read n; do
        tapcat -K "$TAP" -m$count "$n"
        count=$(( $count - 1 ))
        tapedelay $(stat -L -c %s "$n")
    done < <(find "$WDIR" -type f -name 'chnk.*' | sort)
    ( cd "$WDIR" && find . -name 'chnk.*' -type f -exec rm \{\} \; )
}

WDIR=$(mktemp --directory)
trap cleanup EXIT

if [[ $# -lt 2 ]]; then
    echo "Usage: $0 TAP file1 [... fileN]" >&2
    exit 99
fi

TAP="$1"; shift

while [[ $# -gt 0 ]]; do
    NM=$1; shift
    echo "* * * * * * * * Packing $NM"
    size=$(stat -L --format=%s "$NM")
    if [[ $size -gt 32768 ]]; then
        multitape "$NM"
    else
        tapefy "$NM"
        if [[ $# -gt 0 ]]; then
            tapedelay $(stat -L -c %s "$NM")
        fi
    fi
done
echo "Done"

# EOF vim: et:ai:ts=4:sw=4:

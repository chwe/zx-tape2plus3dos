# zx-t2p3

Tape2Plus3 file transfer utility. Target files are unconditionally
overwritten! Press BREAK to exit to BASIC.

See `t2p3-pack.sh` for an example of packing data into a TAP container.

I recommend using a [patched
tapcap](https://gitflic.ru/project/chwe/zx-taptools) utility, it makes
it much easier to prepare data for transfer.

[modeline]: # ( vim: set et:ai:ts=4:sw=2:tw=72: )
